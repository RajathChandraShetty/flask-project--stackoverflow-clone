from celery import Celery
from flask import Flask

def make_celery(app):
    celery = Celery(app.import_name, backend='redis://localhost:6379', broker='redis://localhost:6379')
    
    TaskBase = celery.task
    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery