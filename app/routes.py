from flask import render_template, redirect, url_for, request, flash, g
from app import app, db
from app.models import Question, Answer, Tag, User
from app.forms import NewQusForm, LoginForm, RegistrationForm, AnswerForm, EditProfileForm, SearchForm
from flask_login import login_user, logout_user, current_user, login_required
from datetime import datetime
from app.forms import ResetPasswordRequestForm, ResetPasswordForm
from app.email import send_password_reset_email
from werkzeug.urls import url_parse
from app import celery
import os


@app.before_request
def before_request():
    g.search_form = SearchForm()
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')


@app.route('/answer/<question_id>', methods=['GET', 'POST'])
def answer_question(question_id):
    form = AnswerForm()
    if request.method == 'POST':
        answer = Answer(body=form.ans.data, qus_id=question_id,
                        user_id=current_user.id)
        db.session.add(answer)
        db.session.commit()
        return redirect(url_for('question_detail', question_id=question_id))
    return render_template('ans.html', form=form)


@app.route('/questions/<question_id>', methods=['GET', 'POST'])
def question_detail(question_id):
    form = AnswerForm()
    question = Question.query.filter_by(id=question_id).first()
    answers = Answer.query.filter_by(qus_id=question_id).all()
    if request.method == 'POST':
        answer = Answer(body=form.ans.data, qus_id=question_id,
                        user_id=current_user.id)
        db.session.add(answer)
        db.session.commit()
        return redirect(url_for('question_detail', question_id=question_id))
    return render_template('single_qus.html', question=question, answers=answers, form=form)


@app.route('/questions', methods=['GET', 'POST'])
def question_list():
    questions = Question.query.all()
    answers = []
    for que in questions:
        answers.append(len(Answer.query.filter_by(qus_id=que.id).all()))
    print(answers)
    return render_template('all_qus.html', questions=questions, answers=answers)


@app.route('/new_qus', methods=['GET', 'POST'])
@login_required
def new_qus():

    form = NewQusForm()
    if form.validate_on_submit():
        tags = form.tags.data.split(',')
        qus = Question(title=form.title.data,
                       description=form.description.data, user_id=current_user.id)
        db.session.add(qus)
        db.session.commit()
        for t in tags:
            tag = Tag(name=t)
            tag.ques.append(qus)
            db.session.add(tag)
            db.session.commit()
        return redirect(url_for('question_list'))
    return render_template('ask_qus.html', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if request.method == "POST" and form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        # print('next url is '+next_page)
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', form=form)


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        login_user(user)
        return redirect(url_for('index'))
    return render_template('register.html', title='Register', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route('/users/<username>')
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    questions = user.questions
    answers = user.answers
    print(user.username)
    return render_template('user.html', user=user, questions=questions, answers=answers)


@app.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash('Your profile has been updated.')
        return redirect(url_for('edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', title='Edit Profile', form=form)


@app.route('/q_by_tag/<name>')
def q_by_tag(name):
    tag = Tag.query.filter_by(name=name).first_or_404()
    questions = tag.ques
    #answers = user.answers
    return render_template('q_by_tag.html', tag=tag, questions=questions)


def count_ans(question_id):
    ans_count = Answer.query.filter_by(qus_id=question_id).count()
    print(ans_count)
    return ans_count


@app.route('/reset_password_request', methods=['GET', 'POST'])
def reset_password_request():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = ResetPasswordRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            send_password_reset_email(user)
        flash('Please check your email for the instructions to reset your password')
        return redirect(url_for('login'))
    return render_template('reset_password_request.html',
                           title='Reset Password', form=form)


@app.route('/reset_password/<token>', methods=['GET', 'POST'])
def reset_password(token):
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    user = User.verify_reset_password_token(token)
    if not user:
        return redirect(url_for('index'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        user.set_password(form.password.data)
        db.session.commit()
        flash('Your password has been reset.')
        return redirect(url_for('login'))
    return render_template('reset_password.html', form=form)



@app.route('/search')
def search():
    if not g.search_form.validate():
        return redirect(url_for('questions'))
    page = request.args.get('page', 1, type=int)
    print('this ', g.search_form.q.data)
    questions, total = Question.search(g.search_form.q.data, page,
                               app.config['POSTS_PER_PAGE'])
    next_url = url_for('search', q=g.search_form.q.data, page=page + 1) \
        if total > page * app.config['POSTS_PER_PAGE'] else None
    prev_url = url_for('search', q=g.search_form.q.data, page=page - 1) \
        if page > 1 else None
    return render_template('search.html', title=('Search'), questions=questions,
                           next_url=next_url, prev_url=prev_url)




@app.route('/generate_file/<id>')
@login_required
def generate_file(id):
   task_id = create_log.delay(id)
   print(task_id)
   return {'id' : str(task_id)}

@celery.task(name="tasks.create_log")
def create_log(id):
   current_user = User.query.get(id)
#user = User.query.filter_by(username=username).first_or_404()
   questions = Question.query.filter_by(user_id=current_user.id)
   questions_text = "User : {}\n".format(current_user.username)
   for q in questions:
       questions_text += f'''
       Title : {q.title}
       Description : {q.description}
       Tags : {q.tags}
       ========================================'''
   file_path = os.path.join(os.getcwd(), "app", "static", "{}.txt".format(current_user.username))
   if os.path.exists(file_path):
       os.remove(file_path)
   with open(file_path, "w") as f:
       f.write(questions_text)
   # return {"url" : "/static/{}.txt".format(current_user.id)}
   url = "/static/{}.txt".format(current_user.username)
   # file_location = "/static/{}.txt".format(current_user.id)
   return url
   
@app.route('/get_file/<id>')
def get_file(id):
   task = celery.AsyncResult(id)
   url = task.result
   return {'url' : url}
