import time
from app import celery
from app import *
# app = Celery('tasks', broker='redis://localhost//', backend='redis://localhost//')

@app.celery.task
def reverse(string):
    time.sleep(10)
    return string[::-1]