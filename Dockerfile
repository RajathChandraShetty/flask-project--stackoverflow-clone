FROM python:3.6-alpine

RUN adduser -D stack_overflow
RUN apk update && apk add gcc python3-dev musl-dev
RUN apk add --no-cache build-base zlib-dev

WORKDIR /home/stack_overflow

COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install --upgrade pip
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn

COPY app app
COPY migrations migrations
COPY app.db app.db
COPY stack_overflow.py config.py boot.sh ./
RUN chmod +x boot.sh

ENV FLASK_APP stack_overflow.py

RUN chown -R stack_overflow:stack_overflow ./
USER stack_overflow

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]

