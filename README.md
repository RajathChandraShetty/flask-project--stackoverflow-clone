#TITLE:  StackOverflow- Clone

#Period: Sept-Oct, 2019

* Overview
A simple Q&A app using Flask. This project is for training purpose under MountBlue Technologies, Guided by Mr.Santu Mahapatra


* Goals
To implement StackOverflow like web application which supports most of the features.


* Features
- Create questions and answers.
- Users have a profile (With unique gravatar for each user).
- Support for tagging questions.
- Questions are categorized by most recently asked.
- Supports pagination of questions. 


## Steps to run the app on your machine:
 
$ virtualenv -p /usr/bin/python3 name_of_your_environment
$ source name_of_your_environment/bin/activate
$ pip install -r requirements.txt

** Creating the initial database:

$ flask db init
$ flask db migrate -m "Your message"

** Running the web app :
python stack_overflow.py # The web app can be run in http://127.0.0.1:5000


* Flask
It is a simple web app made using Flask web framework to implement a StackOverflow-like forum site.


* Design
- Used inheritance feature, for eg. every HTML page inherits from base.html.
- Used SQLAlchemy-migrate to keep track of database updates-> Migration feature.
- Used Sqlite database.
- Important feature in this web application is there is a unique gravatar for each user which is generated online when the user successfully registers.
- Pagination is also a feature in our app so that pre-set number of posts per page is visible.
- Used WTF forms which helps in rendering fields and which also has built in validators. 
- Every user can view the dashboard of other user.
- A user can add multiple tags to a particular question.



* Security
- Used SQLALCHEMY prevent SQL injection.
- Used CSRF protction which is auto enabled by WTF forms.


